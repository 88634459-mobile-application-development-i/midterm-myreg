import 'package:flutter/cupertino.dart';

class LatestNews {
  final String header;
  final String description;
  final Icon icon;

  LatestNews({required this.header, required this.description, required this.icon});
}
