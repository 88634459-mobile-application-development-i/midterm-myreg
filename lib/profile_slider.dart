import 'package:flutter/material.dart';
import 'package:my_reg/profileModel.dart';
import 'package:my_reg/profile_row.dart';

class ProfileSlider extends StatelessWidget {
  List<Profile> profile = [
    Profile(
      header: "Student Code",
      details: "69160009",
    ),
    Profile(
      header: "Name",
      details: "Baek Jiheon",
    ),
    Profile(
      header: "Faculty",
      details: "Informatics",
    ),
    Profile(
      header: "Campus",
      details: "Bangsaen",
    ),
    Profile(
      header: "Program",
      details: "2115020 Computer Science",
    ),
    Profile(
      header: "Degree",
      details: "Undergraduate",
    ),
    Profile(
      header: "Admission Academic Year",
      details: "29 / 4 / 2020",
    ),
    Profile(
      header: "Pre Certificate",
      details: "3.16",
    ),
    Profile(
      header: "Credit Attempt",
      details: "95",
    ),
    Profile(
      header: "Credit Passes",
      details: "9.5",
    ),
    Profile(
      header: "GPAX",
      details: "3.53",
    ),
  ];

  ProfileSlider({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 20,
        ),
        ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return ProfileRow(
              profile: profile[index],
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return const SizedBox(
              height: 0,
            );
          },
          itemCount: profile.length,
        ),
      ],
    );
  }
}
