import 'package:flutter/material.dart';
import 'package:my_reg/newsModel.dart';

import 'latest_news_card.dart';

class LatestNewsSlider extends StatelessWidget {
  List<LatestNews> news = [
    LatestNews(
      header: "Your Password Is Expiring",
      description: "You might want to change it   ",
      icon: const Icon(
        Icons.notification_important,
        size: 60,
        color: Colors.grey,
      ),
    ),
    LatestNews(
      header: "Notice On Add - Drop Approve",
      description: "Your add - drop has been approved",
      icon: const Icon(
        Icons.check_box,
        size: 60,
        color: Colors.grey,
      ),
    ),
    LatestNews(
      header: "Alert On Upcoming Final Exam",
      description: "Please check your exam timetable ",
      icon: const Icon(
        Icons.table_chart,
        size: 60,
        color: Colors.grey,
      ),
    ),
    LatestNews(
      header: "Notice On Upcoming Special Holiday",
      description: "The university will close on December 25 ",
      icon: const Icon(
        Icons.calendar_month,
        size: 60,
        color: Colors.grey,
      ),
    ),
  ];

  LatestNewsSlider({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text(
                "Recent News",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Nexa'),
              ),
              Text(
                "View All",
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.amber,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Nexa'),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return LatestNewsCard(
              newscard: news[index],
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return const SizedBox(
              height: 15,
            );
          },
          itemCount: news.length,
        ),
      ],
    );
  }
}
