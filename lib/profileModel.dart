class Profile {
  final String header;
  final String details;

  Profile({required this.header, required this.details});
}
