import 'package:flutter/material.dart';
import 'package:my_reg/main.dart';
import 'package:my_reg/mainBibliography.dart';
import 'mainStudyTimetable.dart';

class RegDrawer extends StatelessWidget {
  const RegDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30),
              ),
              color: Colors.amber,
            ),
            child: Column(
              children: const <Widget>[
                CircleAvatar(
                  radius: 50,
                  backgroundImage: NetworkImage(
                      "https://thumb.zigi.id/frontend/thumbnail/2022/02/10/zigi-6204c8e780b41-jiheon-fromis-9_665_374.jpeg"),
                ),
                SizedBox(height: 8),
                Text(
                  "Baek Jiheon",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Nexa'),
                ),
              ],
            ),
          ),
          ListTile(
            leading: const Icon(Icons.home),
            title: const Text(
              'Home',
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontFamily: 'Nexa',
                color: Color.fromRGBO(137, 137, 137, 1),
              ),
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const mainReg(),
                ),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.emoji_people),
            title: const Text(
              'Bibliography',
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontFamily: 'Nexa',
                color: Color.fromRGBO(137, 137, 137, 1),
              ),
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const mainBibliography(),
                ),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.attach_money),
            title: const Text('Dept & Scholarship',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Nexa',
                    color: Color.fromRGBO(137, 137, 137, 1))),
            onTap: () {
              // Navigate to the home screen
            },
          ),
          ListTile(
            leading: const Icon(Icons.account_circle),
            title: const Text('Graduation Check',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Nexa',
                    color: Color.fromRGBO(137, 137, 137, 1))),
            onTap: () {
              // Navigate to the home screen
            },
          ),
          ListTile(
            leading: const Icon(Icons.fact_check),
            title: const Text('Studied Result',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Nexa',
                    color: Color.fromRGBO(137, 137, 137, 1))),
            onTap: () {
              // Navigate to the home screen
            },
          ),
          ListTile(
            leading: const Icon(Icons.table_chart),
            title: const Text('Study & Exam Timetable',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Nexa',
                    color: Color.fromRGBO(137, 137, 137, 1))),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const mainStudyTimetable(),
                ),
              );
            },
          ),
          Row(
            children: const <Widget>[
              Expanded(
                  child: Divider(
                thickness: 1,
              )),
              SizedBox(width: 10),
              Text("Course management",
                  style: TextStyle(fontSize: 10, color: Colors.grey,fontFamily: 'Nexa',)),
              SizedBox(width: 10),
              Expanded(
                  child: Divider(
                thickness: 1,
              )),
            ],
          ),
          Container(
            color: Colors.transparent,
            child: ListTile(
              leading: Icon(Icons.app_registration),
              title: const Text('Enroll',
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Nexa',
                      color: Color.fromRGBO(137, 137, 137, 1))),
              onTap: () {
                // Navigate to the home screen
              },
            ),
          ),
          ListTile(
            leading: const Icon(Icons.approval),
            title: const Text('Approve Add - Drop',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Nexa',
                    color: Color.fromRGBO(137, 137, 137, 1))),
            onTap: () {
              // Navigate to the home screen
            },
          ),
          Row(
            children: const <Widget>[
              Expanded(
                  child: Divider(
                thickness: 1,
              )),
              SizedBox(width: 10),
              Text("Class details",
                  style: TextStyle(fontSize: 10, color: Colors.grey)),
              SizedBox(width: 10),
              Expanded(
                  child: Divider(
                thickness: 1,
              )),
            ],
          ),
          ListTile(
            leading: const Icon(Icons.people),
            title: const Text('Student list',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Nexa',
                    color: Color.fromRGBO(137, 137, 137, 1))),
            onTap: () {
              // Navigate to the home screen
            },
          ),
          Row(
            children: const <Widget>[
              Expanded(
                  child: Divider(
                thickness: 1,
              )),
              SizedBox(width: 10),
              Text("Logout",
                  style: TextStyle(fontSize: 10, color: Colors.grey,fontFamily: 'Nexa',)),
              SizedBox(width: 10),
              Expanded(
                  child: Divider(
                thickness: 1,
              )),
            ],
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Logout',
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Nexa',
                    color: Color.fromRGBO(137, 137, 137, 1))),
            onTap: () {
              // Navigate to the home screen
            },
          ),
        ],
      ),
    );
  }
}
