import 'dart:ui';
import 'package:flutter/cupertino.dart';

class QuickAccess {
  final String description;
  final Color color;
  final Icon icon;

  QuickAccess({required this.description, required this.color, required this.icon});
}

