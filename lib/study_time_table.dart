import 'package:flutter/material.dart';
import 'package:my_reg/quickAccessModel.dart';
import 'package:my_reg/subjectModel.dart';

import 'mainBibliography.dart';

class StudyTimeTable extends StatelessWidget {
  const StudyTimeTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Course> mondayCourses = [
      Course(
        courseName: "Software Testing",
        courseID: "88624559",
        section: 2,
        buildingRoom: "IF-4M210",
        time: "10:00-12:00",
        color: Color.fromRGBO(181, 170, 153, 1),
      ),
      Course(
        courseName: "Object-Oriented Analysis and Design",
        courseID: "88624459",
        section: 2,
        buildingRoom: "IF-3M210",
        time: "13:00-15:00",
        color: Color.fromRGBO(0, 208, 156, 1),
      ),
      Course(
        courseName: "Web Programming",
        courseID: "88624359",
        section: 2,
        buildingRoom: "IF-3M210",
        time: "17:00-19:00",
        color: Color.fromRGBO(152, 176, 169, 1),
      ),
    ];
    List<Course> tuesdayCourses = [
      Course(
        courseName: "Multimedia Programming for Multiplatforms",
        courseID: "88634259",
        section: 2,
        buildingRoom: "IF-4C02",
        time: "10:00-12:00",
        color: Color.fromRGBO(68, 159, 147, 1),
      ),
      Course(
        courseName: "Software Testing",
        courseID: "88624559",
        section: 2,
        buildingRoom: "IF-3C03",
        time: "13:00-15:00",
        color: Color.fromRGBO(181, 170, 153, 1),
      ),
      Course(
        courseName: "Object-Oriented Analysis and Design",
        courseID: "88624459",
        section: 2,
        buildingRoom: "IF-3M210",
        time: "17:00-19:00",
        color: Color.fromRGBO(0, 208, 156, 1),
      ),
    ];
    List<Course> wednesdayCourses = [
      Course(
        courseName: "Mobile Application Development I",
        courseID: "88634459",
        section: 2,
        buildingRoom: "IF-4C02",
        time: "10:00-12:00",
        color: Color.fromRGBO(224, 166, 0, 1),
      ),
      Course(
        courseName: "Multimedia Programming for Multiplatforms",
        courseID: "88634259",
        section: 2,
        buildingRoom: "IF-4C01",
        time: "13:00-15:00",
        color: Color.fromRGBO(68, 159, 147, 1),
      ),
      Course(
        courseName: "Web Programming",
        courseID: "88624359",
        section: 2,
        buildingRoom: "IF-3C01",
        time: "15:00-17:00",
        color: Color.fromRGBO(152, 176, 169, 1),
      ),
    ];
    List<Course> fridayCourses = [
      Course(
        courseName: "Introduction to Natural Language Processing",
        courseID: "88646259",
        section: 2,
        buildingRoom: "IF-5T05",
        time: "09:00-12:00",
        color: Color.fromRGBO(255, 84, 56, 1),
      ),
      Course(
        courseName: "Mobile Application Development I",
        courseID: "88634459",
        section: 2,
        buildingRoom: "IF-4C02",
        time: "13:00-15:00",
        color: Color.fromRGBO(224, 166, 0, 1),
      ),
    ];
    return SizedBox(
        height: 590,
        child: Column(
          children: [
            SizedBox(
              height: 50,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: const [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 15),
                    child: Text(
                      "Monday",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 90,
              child: ListView.separated(
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      child: Container(
                        width: 200,
                        decoration: BoxDecoration(
                          color: mondayCourses[index].color,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(169, 176, 185, 0.4),
                              spreadRadius: 0,
                              blurRadius: 8.0,
                              offset: Offset(0, 2),
                            )
                          ],
                        ),
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 12,
                        ),
                        child: Column(
                          children: [
                            Text(
                              mondayCourses[index].courseName.length > 16
                                  ? mondayCourses[index]
                                          .courseName
                                          .substring(0, 16) +
                                      "..."
                                  : mondayCourses[index].courseName,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              mondayCourses[index].courseID,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              mondayCourses[index].time,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              mondayCourses[index].buildingRoom,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      width: 15.0,
                    );
                  },
                  itemCount: mondayCourses.length),
            ),
            SizedBox(
              height: 50,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: const [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 15),
                    child: Text(
                      "Tuesday",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 90,
              child: ListView.separated(
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      child: Container(
                        width: 200,
                        decoration: BoxDecoration(
                          color: tuesdayCourses[index].color,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(169, 176, 185, 0.4),
                              spreadRadius: 0,
                              blurRadius: 8.0,
                              offset: Offset(0, 2),
                            )
                          ],
                        ),
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 12,
                        ),
                        child: Column(
                          children: [
                            Text(
                              tuesdayCourses[index].courseName.length > 16
                                  ? tuesdayCourses[index]
                                          .courseName
                                          .substring(0, 16) +
                                      "..."
                                  : tuesdayCourses[index].courseName,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              tuesdayCourses[index].courseID,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              tuesdayCourses[index].time,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              tuesdayCourses[index].buildingRoom,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      width: 15.0,
                    );
                  },
                  itemCount: tuesdayCourses.length),
            ),
            SizedBox(
              height: 50,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: const [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 15),
                    child: Text(
                      "Wednesday",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 90,
              child: ListView.separated(
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      child: Container(
                        width: 200,
                        decoration: BoxDecoration(
                          color: wednesdayCourses[index].color,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(169, 176, 185, 0.4),
                              spreadRadius: 0,
                              blurRadius: 8.0,
                              offset: Offset(0, 2),
                            )
                          ],
                        ),
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 12,
                        ),
                        child: Column(
                          children: [
                            Text(
                              wednesdayCourses[index].courseName.length > 16
                                  ? wednesdayCourses[index]
                                          .courseName
                                          .substring(0, 16) +
                                      "..."
                                  : wednesdayCourses[index].courseName,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              wednesdayCourses[index].courseID,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              wednesdayCourses[index].time,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              wednesdayCourses[index].buildingRoom,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      width: 15.0,
                    );
                  },
                  itemCount: wednesdayCourses.length),
            ),
            SizedBox(
              height: 50,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: const [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 15),
                    child: Text(
                      "Friday",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 90,
              child: ListView.separated(
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      child: Container(
                        width: 200,
                        decoration: BoxDecoration(
                          color: fridayCourses[index].color,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(169, 176, 185, 0.4),
                              spreadRadius: 0,
                              blurRadius: 8.0,
                              offset: Offset(0, 2),
                            )
                          ],
                        ),
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 12,
                        ),
                        child: Column(
                          children: [
                            Text(
                              fridayCourses[index].courseName.length > 16
                                  ? fridayCourses[index]
                                          .courseName
                                          .substring(0, 16) +
                                      "..."
                                  : fridayCourses[index].courseName,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              fridayCourses[index].courseID,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              fridayCourses[index].time,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              fridayCourses[index].buildingRoom,
                              style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      width: 15.0,
                    );
                  },
                  itemCount: fridayCourses.length),
            ),
          ],
        ));
  }
}
