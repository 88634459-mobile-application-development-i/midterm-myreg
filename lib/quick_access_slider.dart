import 'package:flutter/material.dart';
import 'package:my_reg/mainStudyTimetable.dart';
import 'package:my_reg/quickAccessModel.dart';

import 'mainBibliography.dart';

class QuickAccessSlider extends StatelessWidget {
  const QuickAccessSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<QuickAccess> quickaccess = [
      QuickAccess(
        description: "Bibliography",
        color: const Color.fromRGBO(0, 110, 97, 1),
        icon: const Icon(
          Icons.emoji_people,
          color: Colors.white,
          size: 60,
        ),
      ),
      QuickAccess(
        description: "Study & Exam\nTimetable",
        color: const Color.fromRGBO(181, 170, 153, 1),
        icon: const Icon(
          Icons.table_chart,
          color: Colors.white,
          size: 60,
        ),
      ),
    ];
    return SizedBox(
      height: 90,
      child: ListView.separated(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.symmetric(horizontal: 24),
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                if (quickaccess[index].description == "Bibliography") {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const mainBibliography(),
                    ),
                  );
                } else if (quickaccess[index].description == "Study & Exam\nTimetable") {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const mainStudyTimetable(),
                    ),
                  );
                }
              },
              child: Container(
                width: 200,
                decoration: BoxDecoration(
                  color: quickaccess[index].color,
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: const [BoxShadow(
                      color: Color.fromRGBO(169, 176, 185, 0.4),
                      spreadRadius: 0,
                      blurRadius: 8.0,
                      offset: Offset(0, 2),
                    )
                  ],
                ),
                padding: const EdgeInsets.symmetric(
                  vertical: 16,
                  horizontal: 12,
                ),
                child: Stack(
                  children: [
                    Positioned(
                      right: 0,
                      top: 5,
                      child: Opacity(
                        opacity: 0.7,
                        child: quickaccess[index].icon,
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: quickaccess[index].description,
                            style: const TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Nexa'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return const SizedBox(
              width: 15.0,
            );
          },
          itemCount: quickaccess.length),
    );
  }
}
