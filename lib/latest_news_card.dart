import 'package:flutter/material.dart';
import 'package:my_reg/newsModel.dart';

class LatestNewsCard extends StatelessWidget {
  final LatestNews newscard;

  LatestNewsCard({required this.newscard});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        height: 100,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: const Color.fromRGBO(220, 233, 245, 1),
          ),
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 16,
        ),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              top: 5,
              child: Opacity(
                opacity: 0.7,
                child: newscard.icon,
              ),
            ),
            Column(
              children: [
                Text(
                  newscard.header,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  newscard.description,
                  style: const TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
