import 'package:flutter/material.dart';
import 'package:my_reg/profileModel.dart';

class ProfileRow extends StatelessWidget {
  final Profile profile;

  ProfileRow({required this.profile});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        height: 50,
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 16,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                profile.header,
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontFamily: 'Nexa',
                    fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                profile.details,
                style: const TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                    fontFamily: 'Nexa',
                    fontWeight: FontWeight.normal),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
