import 'dart:ui';

class Course {
  final String courseName;
  final String courseID;
  final int section;
  final String buildingRoom;
  final String time;
  final Color color;

  Course({required this.courseName, required this.courseID, required this.section, required this.buildingRoom, required this.time, required this.color});
}

